﻿import os, re

__all__ = [
    "create_folder",
    "convert_command_line_arg",
    "parse_command_line_args",
]

def create_folder(s_path):
    ''' Create a folder if it doesn't already exist,
        even if multiple nested folders must be created
        Arguments:
            s_path: {str} of the folder path to be created (must end in separator if last element is folder)
        Returns:
            {bool} if folder is successfully made
    '''
    if os.path.exists(s_path):
        return True
    s_drive, s_folder_path = os.path.splitdrive(s_path)
    s_folder_path, s_file = os.path.split(s_folder_path)
    s_drive, s_folder_path, s_file
    
    if os.path.altsep:
        re_path_split = re.compile(r"[{}{}]".format(re.escape(os.path.sep), re.escape(os.path.altsep)))
    else:
        re_path_split = re.compile(r"[{}]".format(re.escape(os.path.sep)))
    
    a_folders = [
        x for x in re_path_split.split(s_folder_path)
        if x]
    
    for i_len in range(1, len(a_folders) + 1):
        s_check_path = os.path.join(s_drive, os.path.sep, *a_folders[:i_len])
        if not os.path.isdir(s_check_path):
            os.mkdir(s_check_path)
    
    return os.path.isdir(os.path.join(s_drive, os.path.sep, s_folder_path))


def convert_command_line_arg(s_value, b_bool=True, b_int=True, b_float=True):
    ''' Convert the value of a str command line argument value into bool, int, float or list if appropriate '''
    if isinstance(s_value, list):
        return [convert_command_line_arg(x, b_bool=b_bool, b_int=b_int, b_float=b_float) for x in s_value]
    elif isinstance(s_value, tuple):
        return tuple([convert_command_line_arg(x, b_bool=b_bool, b_int=b_int, b_float=b_float) for x in s_value])
    elif not isinstance(s_value, str):
        return s_value
    
    s_value = s_value.strip()
    if s_value.lower() in {'yes', 'true', 'on'}:
        if b_bool: return True
        else: return s_value
    elif s_value.lower() in {'no', 'false', 'off'}:
        if b_bool: return False
        else: return s_value
    elif b_int or b_float and re.search(r"\d", s_value) and not re.search(r"[a-zA-Z]", s_value):
        if b_int and re.match(r"^(\d{1,3},)*\d+$", s_value):
            return int(re.sub(r"[^\d]", "", s_value))
        elif b_float and re.match(r"^((\d{1,3},)*\d+(\.\d*)?|.\d+)$", s_value):
            return float(re.sub(r"[^\d.]", "", s_value))
        else:
            return s_value
    else:
        return s_value
    

def parse_command_line_args(sys_argv):
    ''' parse the command line arguments and keyword arguments into a list and dict
        Arguments:
            {list} from sys.argv
        Returns:
            {tuple} of ({list} arguments, {dict} keyword arguments)
    '''
    a_args, e_kwargs = [], {}
    
    idx = 1
    while idx < len(sys_argv):
        #print(idx, sys_argv[idx])
        if sys_argv[idx].startswith('--'):
            s_key = sys_argv[idx][2:]
            #print(f'sys_argv[{idx}]: "{s_key}"')
            if ":" in s_key:
                if re.search(r": *[\"'([]? *$", s_key):  # only the key was captured, not the value
                    s_key = s_key.strip()
                    #print(f'only key in sys_argv[{idx}]: "{s_key}"')
                    a_values = []
                    s_type = ""
                    if s_key[-1] == ":":
                        s_key = s_key[:-1]
                        while idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):  # no indication of start of list, and next item(s) could be value
                            idx += 1
                            val = sys_argv[idx].strip()
                            if val: a_values.append(val)
                    elif s_key[-1] == "[":
                        s_key = re.sub(r": *\[$", "", s_key[:-1])
                        s_type = "list"
                        while idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):  # start of list and and next item(s) could be value
                            idx += 1
                            val = sys_argv[idx].strip()
                            if "]" in val:
                                val = re.sub(r"\].*$", "", val)
                                sys_argv[idx] = re.sub(r"^.*\]", "", sys_argv[idx])
                                if val: a_values.append(val)
                                idx -= 1
                                break
                            else:
                                if val: a_values.append(val)
                    elif s_key[-1] == "(":
                        s_key = re.sub(r": *\($", "", s_key[:-1])
                        s_type = "tuple"
                        while idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):  # start of list and and next item(s) could be value
                            idx += 1
                            val = sys_argv[idx].strip()
                            if ")" in val:
                                val = re.sub(r"\).*$", "", val)
                                sys_argv[idx] = re.sub(r"^.*\)", "", sys_argv[idx])
                                if val: a_values.append(val)
                                idx -= 1
                                break
                            else:
                                if val: a_values.append(val)
                    #else:
                    #    if idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):
                        
                        
                    if a_values:
                        if a_values[0].startswith("["):
                            a_values[0] = a_values[0][1:]
                            s_type = "list"
                        elif a_values[0].startswith("("):
                            a_values[0] = a_values[0][1:]
                            s_type = "tuple"
                        for i_val, s_val in enumerate(a_values):
                            #print(f'a_values[{i_val}] == "{s_val}"')
                            if s_val.endswith("]"):
                                a_values[i_val] = s_val[:-1]
                                if i_val < len(a_values) - 1:
                                    idx -= ((len(a_values) - 1) - i_val)
                            elif s_val.endswith(")"):
                                a_values[i_val] = s_val[:-1]
                                if i_val < len(a_values) - 1:
                                    idx -= ((len(a_values) - 1) - i_val)
                                    
                    if s_type == "list":
                        val = a_values
                    elif s_type == "tuple":
                        val = tuple(a_values)
                    elif s_type == "" and len(a_values) == 1:
                        val = a_values[0]
                    else:
                        val = a_values
                    
                    e_kwargs[s_key] = val
                        
                else:
                    s_key, val = s_key.split(':', 1)
                    val = val.strip()
                    if val.startswith(("[", "(")):
                        s_start = val[0]
                        if s_start == "[" and val.endswith("]"):
                            a_values = val[1:-1].split(",")
                            s_type = "list"
                        elif s_start == "(" and val.endswith(")"):
                            a_values = tuple(val[1:-1].split(","))
                            s_type = "tuple"
                        else:
                            if s_start == "(":
                                s_type = "tuple"
                            elif s_start == "[":
                                s_type = "list"
                            
                            #print("val", val)
                            a_values = [x.strip() for x in val[1:].split(",") if x.strip()]
                            
                            while idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):  # next item(s) could be more values
                                idx += 1
                                val = sys_argv[idx].strip()
                                if s_type == "tuple" and ")" in val:
                                    val = re.sub(r"\).*$", "", val)
                                    sys_argv[idx] = re.sub(r"^.*\)", "", sys_argv[idx])
                                    if val: a_values.append(val)
                                    idx -= 1
                                    break
                                elif s_type == "list" and "]" in val:
                                    val = re.sub(r"\].*$", "", val)
                                    sys_argv[idx] = re.sub(r"^.*\]", "", sys_argv[idx])
                                    if val: a_values.append(val)
                                    idx -= 1
                                    break
                                else:
                                    if val: a_values.append(val)
                        
                        if s_type == "list":
                            val = a_values
                        elif s_type == "tuple":
                            val = tuple(a_values)
                        elif s_type == "" and len(a_values) == 1:
                            val = a_values[0]
                        else:
                            val = a_values
                    
                    if s_key in e_kwargs:
                        if isinstance(e_kwargs[s_key], list):
                            e_kwargs[s_key].append(val)
                        else:
                            e_kwargs[s_key] = [e_kwargs[s_key], val]
                    else:
                        e_kwargs[s_key] = val
                    
            elif idx + 1 < len(sys_argv) and not sys_argv[idx + 1].startswith('--'):
                val = sys_argv[idx + 1]
                if s_key in e_kwargs:
                    if isinstance(e_kwargs[s_key], list):
                        e_kwargs[s_key].append(val)
                    else:
                        e_kwargs[s_key] = [e_kwargs[s_key], val]
                else:
                    e_kwargs[s_key] = sys_argv[idx + 1]
                idx += 1
            
            elif s_key not in e_kwargs:
                if ':' in s_key:
                    s_key, val = s_key.split(':', 1)
                else:
                    val = None
                
                if s_key in e_kwargs and val:
                    if isinstance(e_kwargs[s_key], list):
                        e_kwargs[s_key].append(val)
                    else:
                        e_kwargs[s_key] = [e_kwargs[s_key], val]
                else:
                    e_kwargs[s_key] = val
                
        else:
            a_args.append(convert_command_line_arg(sys_argv[idx]))
        idx += 1
        
    e_kwargs = {key.lower(): convert_command_line_arg(val) for key, val in e_kwargs.items()}
    
    return a_args, e_kwargs

