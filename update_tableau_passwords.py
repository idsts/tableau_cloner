﻿import os, sys, re
import json

try:
    from . import tableau_api, utils, aws_secrets
except ImportError:
    import tableau_api, utils, aws_secrets


def update_all_tableau_project_rds_passwords(e_all_rds_secrets=None, s_secret_region=aws_secrets.s_SECRET_DEFAULT_REGION):
    if not e_all_rds_secrets:
        e_all_rds_secrets = aws_secrets.get_all_rds_secrets(s_secret_region)
    
    o_tab = tableau_api.TableauProject()
    
    a_all_projects = o_tab.get_all_projects()
    for s_proj_id, o_proj in a_all_projects.items():
        s_parent = a_all_projects[o_proj.parent_id].name if o_proj.parent_id else ""
        if not o_proj.parent_id:
            print(f"Checking RDS connections for {o_proj.name}\t\t{s_proj_id}")
            e_project = o_tab.get_whole_project(s_proj_id, refresh=False)
            update_project_rds_passwords(e_project, e_all_rds_secrets, o_tab._server)
            o_tab.reconnect()
    


def update_project_rds_passwords(e_project, e_rds_secrets, o_server, **kwargs):
    for s_fl, o_fw in e_project.get("flows", {}).items():
        for o_conn in o_fw.connections:
            #print("Flow Connection", o_conn.server_address)
            if o_conn.server_address in e_rds_secrets:
                e_secret = e_rds_secrets[o_conn.server_address]
                print(f"\nUpdating secret for flow connection {o_conn.server_address}")
                if kwargs.get("b_debug"): print(e_secret)
                o_conn.username = e_secret["username"]
                o_conn.password = e_secret["password"]
                o_conn.embed_password = True
                o_server.flows.update_connection(o_fw, o_conn)
    
    for s_ds, o_ds in e_project.get("data_sources", {}).items():
        for o_conn in o_ds.connections:
            #print("Datasource Connection", o_conn.server_address)
            if o_conn.server_address in e_rds_secrets:
                e_secret = e_rds_secrets[o_conn.server_address]
                print(f"\nUpdating secret for datasource connection {o_conn.server_address}")
                if kwargs.get("b_debug"): print(e_secret)
                o_conn.username = e_secret["username"]
                o_conn.password = e_secret["password"]
                o_conn.embed_password = True
                o_server.datasources.update_connection(o_ds, o_conn)
    
    for s_wb, o_wb in e_project.get("workbooks", {}).items():
        for o_conn in o_wb.connections:
            #print("Workbook Connection", o_conn.server_address)
            if o_conn.server_address in e_rds_secrets:
                e_secret = e_rds_secrets[o_conn.server_address]
                print(f"\nUpdating secret for workbook connection {o_conn.server_address}")
                if kwargs.get("b_debug"): print(e_secret)
                o_conn.username = e_secret["username"]
                o_conn.password = e_secret["password"]
                o_conn.embed_password = True
                o_server.workbooks.update_connection(o_wb, o_conn)
    
    for s_child, e_child_project in e_project.get("child_projects", {}).items():
        #print("e_child_project", e_child_project)
        update_project_rds_passwords(e_child_project, e_rds_secrets, o_server)


if __name__ == "__main__":
    args, kwargs = utils.parse_command_line_args(sys.argv)
    print("Running update_tableau_passwords.py from shell")
    print(args)
    print(kwargs)
    update_all_tableau_project_rds_passwords(*args, **kwargs)
