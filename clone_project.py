import os, sys, re
import json
from collections import defaultdict

try:
    from . import tableau_api, utils
except ImportError:
    import tableau_api, utils

o_TAB = None

s_STAGING_TEMPLATE_SOURCE_PROJECT_ID = "4c3ba623-be60-4117-a4af-a2530faa3612"
s_PRODUCTION_TEMPLATE_SOURCE_PROJECT_ID = "fe12d956-6578-4b49-89e5-dae91bc85e92"

s_STAGING_TEMPLATE_SOURCE_CONNECTION = "cw2-demo-vaccineconsent-postgres-primary.cb0othkj2tys.us-east-1.rds.amazonaws.com"
s_PRODUCTION_TEMPLATE_SOURCE_CONNECTION= "cw2-prod-mn-postgres-primary.cfhrhrcec5gx.us-east-2.rds.amazonaws.com"

s_STAGING_TEMPLATE_SOURCE_NAME = "Staging"
s_PRODUCTION_TEMPLATE_SOURCE_NAME = "Minnesota"

s_STAGING_TEMPLATE_SOURCE_ABBR = "TEST"
s_PRODUCTION_TEMPLATE_SOURCE_ABBR = "MN"

'''
    Command line input should look something like:
    python clone_project.py 4c3ba623-be60-4117-a4af-a2530faa3612 Staging Oregon TEST OR --old_val_1:new_val_1 --old_val_2:new_val_2
    
    Where:
        "4c3ba623-be60-4117-a4af-a2530faa3612" would be the id of the project to clone
        "Minnesota" is the old name/state to swap out with the new name/state
        "Maryland" is the new name/state to replace "Staging" with or insert
        "MN" is the old abbreviation to swap out with the new abbreviation
        "MD" is the new abbreviation to replace "TEST" with or insert
        --old_val_1:new_val_1 is a key combination of an old value that appears in
                              the configuration or file for a project/datasource/flow/workbook
                              and a new value that will be used in its place when creating the cloned object
        --old_val_2:new_val_2 is another key combination of an old value that appears in
                              the configuration or file for a project/datasource/flow/workbook
                              and a new value that will be used in its place when creating the cloned object
'''


def clone_project(source_project_id="", old_name="", new_name="", old_abbr="", new_abbr="", e_old_new_maps=None, e_connection_credentials=None, o_replacer=None, **kwargs):

    if o_replacer is None:
        o_replacer = tableau_api.ReplaceState(s_old_name=old_name, s_new_name=new_name, s_old_abbr=old_abbr, s_new_abbr=new_abbr)
    
    # Download the project configurations to local memory, and return the selected source project as a dictionar
    e_source_project = o_TAB.get_whole_project(source_project_id, refresh=True)
    
    s_target_name = o_replacer.replace_or_insert(e_source_project["project"].name)
    
    for s_proj_id, o_proj in o_TAB.all_projects.items():
        if o_proj.name == s_target_name:
            target_project_id = o_proj.id
            
            return update_project(
                source_project_id,
                target_project=target_project_id,
                old_name=old_name,
                new_name=new_name,
                old_abbr=old_abbr,
                new_abbr=new_abbr,
                e_old_new_maps=e_old_new_maps,
                e_connection_credentials=e_connection_credentials,
                o_replacer=o_replacer,
                **kwargs)
    
    print("\n\nStarting duplication")
    # Prep duper with values that will be substituted from the old objects to their new clones
    o_DUPE = tableau_api.TableauProjectDuper(
        e_source_project,
        e_old_new_maps,
        s_old_name = old_name,
        s_new_name = new_name,
        s_old_abbr = old_abbr,
        s_new_abbr = new_abbr,
        e_connection_credentials = e_connection_credentials,
    )
    o_DUPE.connect()
    
    print("\nDuplicating project objects")
    # Duplicate the project objects for main project and its sub-projects
    o_DUPE.duplicate_project_objects()
    
    o_DUPE.reconnect()
    print("\nDuplicating project datasources")
    # Duplicate the datasource objects for main project and its sub-projects
    o_DUPE.duplicate_project_datasources()
    
    print("\nDuplicating project flows")
    # Duplicate the flow objects for main project and its sub-projects
    o_DUPE.duplicate_project_flows()
    
    o_DUPE.reconnect()
    print("\nDuplicating project workbooks")
    # Duplicate the workbook objects for main project and its sub-projects
    try:
        o_DUPE.duplicate_project_workbooks()
    except Exception as e:
        print(e)
        print("Unable to duplicate workbook in this environment. Check permissions/access/VPC.")
        o_DUPE.duplicate_project_workbooks(skip_update=True)
    
    print("Replacement values\n")
    print(o_DUPE._old_new_maps)
    
    print("\n\nProject Duplicated")

    return {
        "o_DUPE": o_DUPE,
        "e_old_new_maps": e_old_new_maps,}

def update_project(source_project_id="", target_project="", old_name="", new_name="", old_abbr="", new_abbr="", e_old_new_maps=None, e_connection_credentials=None, o_replacer=None, **kwargs):
    assert target_project
    
    e_all_projects = o_TAB.get_all_projects(refresh=True)
    
    # Download the project configurations to local memory, and return the selected source project as a dictionar
    e_source_project = o_TAB.get_whole_project(source_project_id, refresh=False)
    
    if o_replacer is None:
        o_replacer = tableau_api.ReplaceState(s_old_name=old_name, s_new_name=new_name, s_old_abbr=old_abbr, s_new_abbr=new_abbr)
    
    target_project_id = ""
    if target_project in e_all_projects:
        target_project_id = target_project
    else:
        #s_replaced_name = o_replacer.replace_or_insert(e_source_project["project"].name)
        for s_proj_id, o_proj in e_all_projects.items():
            if o_proj.name == target_project:
                target_project_id = o_proj.id
                break
    assert target_project_id
    
    e_cloned_project = o_TAB.get_whole_project(target_project_id, refresh=False)
    
    e_old_new_maps = {
        source_project_id: target_project_id,
        s_STAGING_TEMPLATE_SOURCE_CONNECTION: "cw2-staging-internal-postgres-primary.cb0othkj2tys.us-east-1.rds.amazonaws.com",
        s_PRODUCTION_TEMPLATE_SOURCE_CONNECTION: "cw2-staging-internal-postgres-primary.cb0othkj2tys.us-east-1.rds.amazonaws.com",
    }
    if kwargs.get("s_old_host") and kwargs.get("s_host"):
        e_old_new_maps[kwargs["s_old_host"]] = kwargs["s_host"]
    
    e_cloned_object_by_type = defaultdict(list)
    e_maps_by_type = defaultdict(dict)
    
    tableau_api.find_old_new_maps(
        e_source_project,
        e_cloned_project,
        e_old_new_maps,
        o_replacer,
        e_cloned_object_by_type=e_cloned_object_by_type,
        e_maps_by_type=e_maps_by_type,
        b_flows=True,
        b_datasources=True,
        b_workbooks=True,
        b_views=True,
        b_connections=True)
    
    print(len(e_old_new_maps))
    
    e_remaps = defaultdict(list)
    e_remaps[source_project_id].append(target_project_id)
    for s_object, e_maps in e_maps_by_type.items():
        for s_old_id, s_replaced_id in e_maps.items():
            e_remaps[s_old_id].append(s_replaced_id)
    print("e_remaps", e_remaps)
    
    e_old_new_maps.update(e_maps_by_type.get("projects", {}))
    
    tableau_api.delete_objects(o_TAB._server, e_cloned_object_by_type, b_flows=True, b_datasources=True, b_workbooks=True)
    
    print("\n\nStarting update")
    # Prep duper with values that will be substituted from the old objects to their new clones
    o_DUPE = tableau_api.TableauProjectDuper(
        e_source_project,
        e_old_new_maps,
        e_remaps = e_remaps,
        s_old_name = old_name,
        s_new_name = new_name,
        s_old_abbr = old_abbr,
        s_new_abbr = new_abbr,
        e_connection_credentials = e_connection_credentials,
    )
    o_DUPE.connect()
    
    print("\nDuplicating project objects")
    # Duplicate the project objects for main project and its sub-projects
    o_DUPE.duplicate_project_objects(e_parent_structure=e_cloned_project)
    
    o_DUPE.reconnect()
    print("\nDuplicating project datasources")
    # Duplicate the datasource objects for main project and its sub-projects
    o_DUPE.duplicate_project_datasources(e_parent_structure=e_cloned_project)
    
    print("\nDuplicating project flows")
    # Duplicate the flow objects for main project and its sub-projects
    o_DUPE.duplicate_project_flows(e_parent_structure=e_cloned_project)
    
    o_DUPE.reconnect()
    print("\nDuplicating project workbooks")
    # Duplicate the workbook objects for main project and its sub-projects
    try:
        o_DUPE.duplicate_project_workbooks(e_parent_structure=e_cloned_project)
    except Exception as e:
        print(e)
        print("Unable to duplicate workbook in this environment. Check permissions/access/VPC.")
        o_DUPE.duplicate_project_workbooks(e_parent_structure=e_cloned_project, skip_update=True)
    
    print("Replacement values\n")
    print(o_DUPE._old_new_maps)
    
    print("\n\nProject Duplicated")
    
    return {
        "o_DUPE": o_DUPE,
        "e_old_new_maps": e_old_new_maps,
        "e_remaps": e_remaps,
        "e_cloned_object_by_type": e_cloned_object_by_type,
        "e_maps_by_type": e_maps_by_type,}

def main(*args, **kwargs):
    args = list(args)
    
    s_old_project_id = ""
    s_target_project_id = ""
    s_replacement_value_json = ""
    if args:
        for i, x in enumerate(args):
            if str(x).lower().endswith(".json"):
                s_replacement_value_json = x
                args.pop(i)
                break
    #print('s_replacement_value_json', s_replacement_value_json)
    
    if args and tableau_api.re_UID.search(str(args[0])):
        s_old_project_id = args.pop(0)
    
    if args and tableau_api.re_UID.search(str(args[0])):
        s_target_project_id = args.pop(0)
    
    
    if len(args) >= 4:
        old_name, new_name, old_abbr, new_abbr = args[:4]
    elif len(args) == 2:
        new_name, new_abbr = args
        old_name, old_abbr =  "", ""
    else:
        new_name, new_abbr = "", ""
        old_name, old_abbr =  "", ""
    
    old_name = kwargs.get("old_name", old_name)
    old_abbr = kwargs.get("old_abbr", old_abbr)
    new_name = kwargs.get("new_name", new_name)
    new_abbr = kwargs.get("new_abbr", new_abbr)
    
    e_old_new_maps = {}
    e_old_new_maps.update({
        k: v for k, v in kwargs.items()
        if k not in {
            "connection", "username", "password", "port", "database", "host",
            "old_name", "old_abbr", "new_name", "new_abbr",}
    })
    
    if s_replacement_value_json:
        assert os.path.isfile(s_replacement_value_json)
        e_json = json.load(open(s_replacement_value_json, "r"))
        #print("e_json", e_json)
        
        if e_json.get("old_project"): s_old_project_id = e_json["old_project"]
        if e_json.get("old_name"): old_name = e_json["old_name"]
        if e_json.get("new_name"): new_name = e_json["new_name"]
        if e_json.get("old_abbr"): old_abbr = e_json["old_abbr"]
        if e_json.get("new_abbr"): new_abbr = e_json["new_abbr"]
        
        
        e_connection_credentials = e_json.get("connection_credentials", {})
        e_old_new_maps.update(e_json.get("old_new_maps", {}))
    
    else:
        e_connection_credentials = {}
    
    s_old_host = kwargs.get("old_connection", os.environ.get("SOURCE_POSTGRES_ADDRESS", s_STAGING_TEMPLATE_SOURCE_CONNECTION))
    s_host = kwargs.get("connection", os.environ.get("POSTGRES_ADDRESS", ""))
    
    e_old_new_maps[s_STAGING_TEMPLATE_SOURCE_CONNECTION] = s_host
    e_old_new_maps[s_PRODUCTION_TEMPLATE_SOURCE_CONNECTION] = s_host
    if s_old_host: e_old_new_maps[s_old_host] = s_host
    
    if s_host:
        e_connection_credentials[s_host] = {
            "host": s_host,
            "port": kwargs.get("port", "5432"),
            "database": kwargs.get("database", "postgres"),
            "username": kwargs.get("username", os.environ.get("POSTGRES_USERNAME", "")),
            "password": kwargs.get("password", os.environ.get("POSTGRES_PASSWORD", "")),
        }
    
    global o_TAB
    o_TAB = tableau_api.TableauProject()
    #print(o_TAB._server.version)
    
    a_all_projects = o_TAB.get_all_projects()
    
    if s_PRODUCTION_TEMPLATE_SOURCE_PROJECT_ID in a_all_projects:
        if not s_old_project_id:
            s_old_project_id = s_PRODUCTION_TEMPLATE_SOURCE_PROJECT_ID
        #if not old_connection:
        #    old_connection = s_PRODUCTION_TEMPLATE_SOURCE_CONNECTION
        if not old_name:
            old_name = s_PRODUCTION_TEMPLATE_SOURCE_NAME
        if not old_abbr:
            old_abbr = s_PRODUCTION_TEMPLATE_SOURCE_ABBR
    
    elif s_STAGING_TEMPLATE_SOURCE_PROJECT_ID in a_all_projects:
        if not s_old_project_id:
            s_old_project_id = s_STAGING_TEMPLATE_SOURCE_PROJECT_ID
        #if not old_connection:
        #    old_connection = s_STAGING_TEMPLATE_SOURCE_CONNECTION
        if not old_name:
            old_name = s_STAGING_TEMPLATE_SOURCE_NAME
        if not old_abbr:
            old_abbr = s_STAGING_TEMPLATE_SOURCE_ABBR
    
    s_old_project_id = kwargs.get("old_project", os.environ.get("SOURCE_PROJECT_ID", s_old_project_id))
    s_target_project_id = kwargs.get("target_project", os.environ.get("TARGET_PROJECT", s_target_project_id))
    
    print("old_project_id", s_old_project_id)
    print("old_name", old_name)
    print("new_name", new_name)
    print("old_new_maps", e_old_new_maps)
    print("connection_credentials", e_connection_credentials)
    
    assert s_old_project_id
    assert old_name
    assert new_name
    
    if not new_abbr:
        new_abbr = re.sub("[^A-Z0-9]", "", new_name.upper())
    
    print("old_abbr", old_abbr)
    print("new_abbr", new_abbr)
    
    assert old_abbr
    assert new_abbr
    
    o_replacer = tableau_api.ReplaceState(s_old_name=old_name, s_new_name=new_name, s_old_abbr=old_abbr, s_new_abbr=new_abbr)
    
    if s_target_project_id:
        update_project(s_old_project_id, target_project, old_name, new_name, old_abbr, new_abbr, e_old_new_maps, e_connection_credentials, s_host=s_host, s_old_host=s_old_host, o_replacer=o_replacer)
    else:
        clone_project(s_old_project_id, old_name, new_name, old_abbr, new_abbr, e_old_new_maps, e_connection_credentials, s_host=s_host, s_old_host=s_old_host, o_replacer=o_replacer)


if __name__ == "__main__":
    args, kwargs = utils.parse_command_line_args(sys.argv)
    print("Running clone_project.py from shell")
    print(args)
    print(kwargs)
    main(*args, **kwargs)
