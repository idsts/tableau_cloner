﻿import re
import boto3
import base64
from botocore.exceptions import ClientError
import json

s_SECRET_DEFAULT_REGION = "us-east-1"

def get_all_rds_secrets(region_name=s_SECRET_DEFAULT_REGION):
    # Create a Secrets Manager client
    session = boto3.session.Session()
    secret_client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )
    
    e_secret_response = secret_client.list_secrets()
    a_secret_heads = sorted(e_secret_response.get("SecretList", []), key=lambda x: x["LastChangedDate"])
    
    #a_all_secrets = []
    e_rds_secrets = {}
    for e_sec_head in e_secret_response.get("SecretList", []):
        print(e_sec_head)
        s_arn = e_sec_head.get("ARN", "")
        re_region = re.search("[-a-z0-9]+", re.sub(r"arn:aws:secretsmanager:", "", s_arn, flags=re.I), flags=re.I)
        s_region = re_region.group() if re_region else s_SECRET_DEFAULT_REGION
        #print(s_region)
        e_secret = get_secret(e_sec_head["ARN"], region_name=s_region)
        
        #a_all_secrets.append(e_secret)
        if e_secret.get("username", "") and e_secret.get("password", "") and re.search("\.rds\.", e_secret.get("host", "")):
            e_rds_secrets[e_secret["host"]] = e_secret
    return e_rds_secrets


def get_secret(secret_name="", region_name=s_SECRET_DEFAULT_REGION):
    # Create a Secrets Manager client
    session = boto3.session.Session()
    secret_client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    try:
        get_secret_value_response = secret_client.get_secret_value(
            SecretId=secret_name
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            raise e
    else:
        # Decrypts secret using the associated KMS key.
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
            if secret[0] == "{" and secret[-1] == "}":
                try:
                    secret = json.loads(secret)
                except:
                    pass
        else:
            secret = base64.b64decode(get_secret_value_response['SecretBinary'])
            
    return secret

