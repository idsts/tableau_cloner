﻿import sys, os, re
from collections import defaultdict
from time import time, sleep
from copy import copy, deepcopy
import json
import binascii
import random, string
import xml.etree.ElementTree as ET
from zipfile import ZipFile, ZIP_DEFLATED
import tableauserverclient as TSC  # from https://tableau.github.io/server-client-python/

try:
    from . import utils
except ImportError:
    import utils

re_UID = re.compile(r"^[a-f0-9]{8,8}-[a-f0-9]{4,4}-[a-f0-9]{4,4}-[a-f0-9]{4,4}-[a-f0-9]{12,12}$", flags=re.I)
re_URL_SAFE = re.compile(r"[^A-Z0-9]", flags=re.I)

s_ADDRESS = os.environ.get("TABLEAU_ADDRESS", 'https://tableau.prepmodapp.com')
s_USERNAME = os.environ.get("TABLEAU_USERNAME", '')
s_PASSWORD = os.environ.get("TABLEAU_PASSWORD", '')
s_SITE = ''

assert s_USERNAME
assert s_PASSWORD

s_TMP = os.getcwd()
assert os.path.isdir(s_TMP)

def unzip(s_zip_file, **kwargs):
    '''
        Unzip a zip file into a temporary folder
        Arguments:
            s_zip_file: {str} the path to the zip file
        Returns:
            {list} of absolute paths to the unzipped files.
    '''
    s_tmp_folder = kwargs.get("temp", s_TMP)
    with ZipFile(s_zip_file, 'r') as o_zip:
        a_names = o_zip.namelist()
        o_zip.extractall(s_tmp_folder)
    
    # absolute paths
    a_file_paths = [os.path.join(s_tmp_folder, s_file) for s_file in a_names]
    return a_file_paths


def zip(s_zip_file, a_files, **kwargs):
    o_zip = ZipFile(s_zip_file, 'w', ZIP_DEFLATED)
    for s_file in a_files:
        o_zip.write(s_file, os.path.basename(s_file))
    o_zip.close()
    return o_zip


def generate_uid(a_lengths=[8,4,4,4,12], s_del="-"):
    a_pieces = []
    for i in a_lengths:
        a_pieces.append(binascii.hexlify(os.urandom(int(i / 2))).decode())
    return "-".join(a_pieces)


def random_string_generator(str_size, allowed_chars):
    return ''.join(random.choice(allowed_chars) for x in range(str_size))


def replace_values(item, e_replacements, t_key_path=tuple(), c_uid_paths=set(), a_regexes=[]):
    '''
        Recursively update a nested variable with specified values replaced with new/alternate versions
        
        This is to replace certain values in the cloned configuration dicts, like new IDs for linked items.
        Arguments:
            item: value to check to see if there are applicable replacements
            e_replacements: {dict} of {old_val_1: new_val_1, old_val_2: new_val_2...}
        Returns:
            Variable with any replacements made
    '''
    if item is None:
        return item
    elif isinstance(item, (float, int)):
        return e_replacements.get(item, item)
    elif isinstance(item, str):
        #if re_UID.search(item):
            #print(t_key_path, item, (item and t_key_path in c_uid_paths and item not in e_replacements and re_UID.search(item) is not None))
        if item and t_key_path in c_uid_paths and item not in e_replacements and re_UID.search(item):
            new_item = generate_uid()
            e_replacements[item] = new_item
            return new_item
        else:
            if len(item) > 36 and re_UID.search(item[:36]) and item[:36] in e_replacements:
                return e_replacements[item[:36]] + item[36:]
            else:
                return e_replacements.get(item, item)
    
    elif isinstance(item, dict):
        for k, v in list(item.items()):
            if re_UID.search(k):
                new_item = replace_values(v, e_replacements, t_key_path=t_key_path , c_uid_paths=c_uid_paths)
            else:
                new_item = replace_values(v, e_replacements, t_key_path=t_key_path + (k,), c_uid_paths=c_uid_paths)
            
            new_key = replace_values(k, e_replacements)
            
            #if e_replacements.get(k, k) != k:
            if new_key != k:
                #print(k, e_replacements.get(k, k))
                item[new_key] = new_item
                del item[k]
            else:
                item[k] = new_item
            
        return item
    elif isinstance(item, list):
        return [replace_values(v, e_replacements, t_key_path=t_key_path, c_uid_paths=c_uid_paths) for v in item]
    elif isinstance(item, tuple):
        return tuple([replace_values(v, e_replacements, t_key_path=t_key_path, c_uid_paths=c_uid_paths) for v in item])
    elif isinstance(item, set):
        return {replace_values(v, e_replacements, t_key_path=t_key_path, c_uid_paths=c_uid_paths) for v in item}


def get_url_base(s_url):
    re_base = re.search("(https?://[^/]+)", s_url)
    if re_base:
        return re_base.group()
    else:
        return s_url


def remove_null(item, b_remove_empty=True):
    '''
        Recursively update a nested variable with specified values replaced with new/alternate versions
        
        This is to replace certain values in the cloned configuration dicts, like new IDs for linked items.
        Arguments:
            item: value to check to see if there are applicable replacements
            e_replacements: {dict} of {old_val_1: new_val_1, old_val_2: new_val_2...}
        Returns:
            Variable with any replacements made
    '''
    assert item is not None
    
    if isinstance(item, (float, int, str)):
        return item
    elif isinstance(item, dict):
        return {
            k: remove_null(v)
            for k, v in item.items()
            if v is not None and (not b_remove_empty or remove_null(v))}
    elif isinstance(item, list):
        return [remove_null(v) for v in item if v is not None and (not b_remove_empty or remove_null(v))]
    elif isinstance(item, tuple):
        return tuple([remove_null(v) for v in item if v is not None and (not b_remove_empty or remove_null(v))])
    elif isinstance(item, set):
        return {remove_null(v) for v in item if v is not None and (not b_remove_empty or remove_null(v))}


def replace_xml_values(s_input_file, s_output_file, e_replacements, a_regexes=[], o_replacer=None): #, c_remove_tags=set()):
    o_tree = ET.parse(s_input_file)
    o_root = o_tree.getroot()
    #a_name_elts = o_root.findall(".//name")    # we find all 'name' elements
    #a_elts = o_root.findall()    # we find all 'name' elements
    s_data_source_name = o_root.get("formatted-name", "")
    if s_data_source_name.startswith("federated"):
        s_data_source_name = "federated." + random_string_generator(28, "abcdefghijklmnopqrstuvwxyz0123456789")
        e_replacements[o_root.attrib["formatted-name"]] = s_data_source_name
        o_root.attrib["formatted-name"] = s_data_source_name
    #print(s_data_source_name)
    
    elt_conn = o_tree.find(".//connection/named-connections/named-connection")
    if elt_conn is not None and elt_conn.tag:
        if elt_conn.get("name", "").startswith("postgres"):
            s_old_connection_name = elt_conn.attrib["name"]
            s_connection_name = "postgres." + random_string_generator(28, "abcdefghijklmnopqrstuvwxyz0123456789")
            e_replacements[s_old_connection_name] = s_connection_name
            elt_conn.attrib["name"] = s_connection_name
    
    elt_ds = o_tree.find(".//datasources/datasource")
    if elt_ds is not None and elt_ds.tag:
        if elt_ds.get("name", "").startswith("sqlproxy"):
            s_old_ds_name = elt_ds.attrib["name"]
            s_ds_name = "sqlproxy." + random_string_generator(28, "abcdefghijklmnopqrstuvwxyz0123456789")
            e_replacements[s_old_ds_name] = s_ds_name
            a_regexes.append((re.compile(r"\b{}\b".format(re.escape(s_old_ds_name)), flags=re.I), s_ds_name))
            elt_ds.attrib["name"] = s_ds_name
    
    elt_repo = o_tree.find(".//repository-location")
    if elt_repo is not None and elt_repo.tag:
        #print(elt_repo.tag, elt_repo.attrib.get("id"))
        if elt_repo.attrib.get("id"):
            s_old_repo_name = elt_repo.attrib.get("id")
            if o_replacer:
                s_repo_name = o_replacer.replace_or_insert(s_old_repo_name)
                e_replacements[s_old_repo_name] = s_repo_name
                elt_repo.attrib["id"] = s_repo_name
                a_regexes.append((re.compile(r"\b{}\b".format(re.escape(s_old_repo_name)), flags=re.I), s_repo_name))
                if elt_repo.attrib.get("derived-from"):
                    elt_repo.attrib["derived-from"] = a_regexes[-1][0].sub(a_regexes[-1][1], elt_repo.attrib["derived-from"])
    
    for elt in o_tree.iter():
        if elt.tag == "_.fcp.ObjectModelEncapsulateLegacy.true...object-id" and elt.text.strip() not in e_replacements:
            s_object_id = "[_{}]".format(random_string_generator(32, "0123456789ABCDEF"))
            e_replacements[elt.text.strip()] = s_object_id
            elt.text = s_object_id
        
        for k, v in e_replacements.items():
            if elt.tag:
                elt.tag = elt.tag.replace(k, v)
            if elt.text:
                elt.text = elt.text.replace(k, v)
            
            for item, value in elt.attrib.items():
                elt.attrib[item] = value.replace(k, v)
        
        for re_sub, s_sub in a_regexes:
            if elt.tag:
                elt.tag = re_sub.sub(s_sub, elt.tag)
            if elt.text:
                elt.text = re_sub.sub(s_sub, elt.text)
            
            for item, value in elt.attrib.items():
                elt.attrib[item] = re_sub.sub(s_sub, value)
    
    for item, value in o_root.attrib.items():
        for k, v in e_replacements.items():
            o_root.attrib[item] = value.replace(k, v)

        for re_sub, s_sub in a_regexes:
            o_root.attrib[item] = re_sub.sub(s_sub, value)
    
    o_tree.write(s_output_file, encoding='utf-8', xml_declaration=True)


class TableauProject():
    ''' Class to read/download information about projects in a tableau site '''
    def __init__(self, **kwargs):
        self._username, self._password = kwargs.get("username", s_USERNAME), kwargs.get("password", s_PASSWORD)
        self._address, self._site = kwargs.get("address", s_ADDRESS), kwargs.get("site", s_SITE)
        self._server, self._auth = None, None
        self.connect()
        
        self.all_projects = {}
        self.all_datasources = {}
        self.all_flows = {}
        self.all_workbooks = {}
        self.all_views = {}
        self.all_schedules = {}
        
        self._auto_populate = kwargs.get("auto_populate", True)
    
    def connect(self, **kwargs):
        ''' Connect to the server '''
        s_username, s_password = kwargs.get("username", self._username), kwargs.get("password", self._password)
        s_address, s_site = kwargs.get("address", self._address), kwargs.get("site", self._site)
        self._username, self._password = s_username, s_password
        self._address, self._site = s_address, s_site
    
        self._tableau_auth = TSC.TableauAuth(s_username, s_password, s_site)
        self._server = TSC.Server(s_address, use_server_version=True)
        self._auth = self._server.auth.sign_in(self._tableau_auth)
    
    def reconnect(self, **kwargs):
        i_retries = 0
        while i_retries < 3:
            i_retries += 1
            try:
                self._server.auth.sign_out()
                self._tableau_auth = TSC.TableauAuth(self._username, self._password, self._site)
                self._server.auth.sign_in(self._tableau_auth)
            except:
                print(f"Reconnection attempt {i_retries} failed")
    
    ##### Download all data from server ####
    def refresh(self, **kwargs):
        ''' Clear-out and re-download master lists '''
        self.all_projects = {}
        self.all_datasources = {}
        self.all_flows = {}
        self.all_workbooks = {}
        self.all_views = {}
        self.all_schedules = {}
        
        self.get_all_projects(**kwargs)
        self.get_all_datasources(**kwargs)
        self.get_all_flows(**kwargs)
        self.get_all_schedules(**kwargs)
        self.get_all_workbooks(**kwargs)
        self.get_all_views(**kwargs)
    
    def get_all_sites(self, **kwargs):
        ''' Download the server's sites
        '''
                
        a_all_sites, site_pagination_item = self._server.sites.get()
        print("\nThere are {} sites on the server: ".format(site_pagination_item.total_available))
        
        self.all_sites = {}
        for idx, o_site in enumerate(a_all_sites):
            if kwargs.get("b_debug"):
                print(idx, f"ID:{o_site.id}, Name:{o_site.name}, parent:{o_proj.parent_id}, owner:{o_proj.owner_id}")
            self.all_sites[o_site.id] = o_site
        return self.all_sites
    
    def get_all_projects(self, **kwargs):
        ''' Download the site's projects and populate the properties that aren't automatically downloaded
            such as permissions
        '''
        
        a_all_projects, proj_pagination_item = self._server.projects.get()
        print("\nThere are {} projects on site: ".format(proj_pagination_item.total_available))
        #print([project.name for project in a_all_projects])
        
        self.all_projects = {}
        for idx, o_proj in enumerate(a_all_projects):
            if kwargs.get("populate", self._auto_populate):
                self._server.projects.populate_permissions(o_proj)
                self._server.projects.populate_workbook_default_permissions(o_proj)
                self._server.projects.populate_datasource_default_permissions(o_proj)
                self._server.projects.populate_flow_default_permissions(o_proj)
            
            if kwargs.get("b_debug"):
                print(idx, f"ID:{o_proj.id}, Name:{o_proj.name}, parent:{o_proj.parent_id}, owner:{o_proj.owner_id}")
            
            self.all_projects[o_proj.id] = o_proj
        return self.all_projects
    
    def get_all_datasources(self, project_id=None, **kwargs):
        ''' Download the site's datasources and populate the properties that aren't automatically downloaded
            such as permissions.
            If project_id is specified, return just that project's datasources
            
            Arguments:
                project_id: optional tableau id of a particular project
            Returns:
                {dict} of either all datasources, or just the datasources of the specified project
        '''
        
        a_all_datasources, datasource_pagination_item = self._server.datasources.get()
        print("\nThere are {} datasources on site: ".format(datasource_pagination_item.total_available))
        print([datasource.name for datasource in a_all_datasources])
    
        self.all_datasources = {}
        e_project_datasources = {}
        for idx, o_ds in enumerate(a_all_datasources):
            if kwargs.get("populate", self._auto_populate):
                self._server.datasources.populate_connections(o_ds)
                self._server.datasources.populate_dqw(o_ds)
                self._server.datasources.populate_permissions(o_ds)
            
            if kwargs.get("b_debug"):
                print(f"ID:{o_ds.id}, Name:{o_ds.name}, project:{o_ds.project_name} ({o_ds.project_id}), owner:{o_ds.owner_id}")
            self.all_datasources[o_ds.id] = o_ds
            if project_id and project_id == o_ds.project_id:
                e_project_datasources[o_ds.id] = o_ds
        
        if project_id:
            return e_project_datasources
        else:
            return self.all_datasources
    
    def get_all_flows(self, project_id=None, **kwargs):
        ''' Download the site flows and populate the properties that aren't automatically downloaded
            such as permissions
            If project_id is specified, return just that project's flows
            
            Arguments:
                project_id: optional tableau id of a particular project
            Returns:
                {dict} of either all flows, or just the flows of the specified project
        '''
        
        a_all_flows, flow_pagination_item = self._server.flows.get()
        print("\nThere are {} flows on site: ".format(flow_pagination_item.total_available))
        print([flow.name for flow in a_all_flows])
    
        self.all_flows = {}
        e_project_flows = {}
        for idx, o_flow in enumerate(a_all_flows):
            if kwargs.get("populate", self._auto_populate):
                self._server.flows.populate_connections(o_flow)
                self._server.flows.populate_dqw(o_flow)
                self._server.flows.populate_permissions(o_flow)
            
            if kwargs.get("b_debug"):  
                print(f"ID:{o_flow.id}, Name:{o_flow.name}, project:{o_flow.project_name} ({o_flow.project_id}), owner:{o_flow.owner_id}")
            
            self.all_flows[o_flow.id] = o_flow
            if project_id and project_id == o_flow.project_id:
                e_project_flows[o_flow.id] = o_flow
        
        if project_id:
            return e_project_flows
        else:
            return self.all_flows
    
    def get_all_schedules(self, project_id=None, **kwargs):
        ''' Download the site schedules and populate the properties that aren't automatically downloaded
            such as permissions
            If project_id is specified, return just that project's schedules
            
            Arguments:
                project_id: optional tableau id of a particular project
            Returns:
                {dict} of either all schedules, or just the schedules of the specified project
        '''
        
        a_all_schedules, schedule_pagination_item = self._server.schedules.get()
        print("\nThere are {} schedules on site: ".format(schedule_pagination_item.total_available))
        print([schedule.name for schedule in a_all_schedules])
    
        self.all_schedules = {}
        e_project_schedules = {}
        for idx, o_schedule in enumerate(a_all_schedules):
            #if kwargs.get("populate", self._auto_populate):
                #self._server.schedules.populate_connections(o_schedule)
                #self._server.schedules.populate_dqw(o_schedule)
                #self._server.schedules.populate_permissions(o_schedule)
            
            if kwargs.get("b_debug"):  
                print(f"ID:{o_schedule.id}, Name:{o_schedule.name}, next run:{next_run_at}, project:{o_schedule.project_name} ({o_schedule.project_id}), owner:{o_schedule.owner_id}")
            
            self.all_schedules[o_schedule.id] = o_schedule
            if project_id and project_id == o_schedule.project_id:
                e_project_schedules[o_schedule.id] = o_schedule
        
        if project_id:
            return e_project_schedules
        else:
            return self.all_schedules
    
    def get_all_workbooks(self, project_id=None, **kwargs):
        ''' Download the workbooks and populate the properties that aren't automatically downloaded
            such as permissions
            If project_id is specified, return just that project's workbooks
            
            Arguments:
                project_id: optional tableau id of a particular project
            Returns:
                {dict} of either all workbooks, or just the workbooks of the specified project
        '''
        
        a_all_workbooks, workbook_pagination_item = self._server.workbooks.get()
        print("\nThere are {} workbooks on site: ".format(workbook_pagination_item.total_available))
        print([workbook.name for workbook in a_all_workbooks])
    
        self.all_workbooks = {}
        e_project_workbooks = {}
        for idx, o_wb in enumerate(a_all_workbooks):
            if kwargs.get("populate", self._auto_populate):
                self._server.workbooks.populate_connections(o_wb)
                self._server.workbooks.populate_views(o_wb)
                self._server.workbooks.populate_permissions(o_wb)
            
            if kwargs.get("b_debug"):  
                print(f"ID:{o_wb.id}, Name:{o_wb.name}, project:{o_wb.project_name} ({o_wb.project_id}), owner:{o_wb.owner_id}")
            
            self.all_workbooks[o_wb.id] = o_wb
            if project_id and project_id == o_wb.project_id:
                e_project_workbooks[o_wb.id] = o_wb
        
        if project_id:
            return e_project_workbooks
        else:
            return self.all_workbooks
    
    def get_all_views(self, workbooks=None, **kwargs):
        ''' Download the views and populate the properties that aren't automatically downloaded
            such as permissions
            If workbooks is specified, return just the views belonging to any of the workbooks
            
            Arguments:
                workbooks: {iter} of workbook ids
            Returns:
                {dict} of either all views, or just the views of the specified workbooks
        '''
        
        a_all_views, view_pagination_item = self._server.views.get()
        print("\nThere are {} views on site: ".format(view_pagination_item.total_available))
        print([view.name for view in a_all_views])
    
        self.all_views = {}
        e_workbook_views = defaultdict(dict)
        for idx, o_view in enumerate(a_all_views):
            if kwargs.get("populate", self._auto_populate):
                self._server.views.populate_permissions(o_view)
            
            if kwargs.get("b_debug"):  
                print(f"ID:{o_view.id}, Name:{o_view.name}, workbook:{o_view.workbook_id}, owner:{o_view.owner_id}")
            
            self.all_views[o_view.id] = o_view
            if workbooks and o_view.workbook_id in workbooks:
                e_workbook_views[o_view.workbook_id][o_view.id] = o_view
        
        if workbooks:
            return e_workbook_views
        else:
            return self.all_views
    
    ##### Get particular project information ####
    
    def get_project(self, project_id, **kwargs):
        ''' Get a particular project, refreshing if specified
            Arguments:
                project_id: optional tableau id of a particular project
        '''
        if not self.all_projects or not self.all_projects.get(project_id) or kwargs.get("refresh_projects"):
            self.get_all_projects()
        return self.all_projects.get(project_id, None)
    
    def get_project_datasources(self, project_id, **kwargs):
        ''' Get a particular project's datasources, refreshing if specified
            Arguments:
                project_id: optional tableau id of a particular project
        '''
        assert project_id
        if not self.all_datasources or kwargs.get("refresh_datasources"):
            self.get_all_datasources()
        
        e_project_datasources = {}
        for ds_id, o_ds in self.all_datasources.items():
            if project_id == o_ds.project_id:
                e_project_datasources[o_ds.id] = o_ds
        return e_project_datasources
    
    def get_project_flows(self, project_id, **kwargs):
        ''' Get a particular project's flows, refreshing if specified
            Arguments:
                project_id: optional tableau id of a particular project
        '''
        assert project_id
        if not self.all_flows or kwargs.get("refresh_flows"):
            self.get_all_flows()
        
        e_project_flows = {}
        for flow_id, o_flow in self.all_flows.items():
            if project_id == o_flow.project_id:
                e_project_flows[o_flow.id] = o_flow
        
        return e_project_flows
    
    def get_project_workbooks(self, project_id, **kwargs):
        ''' Get a particular project's workbooks, refreshing if specified
            Arguments:
                project_id: optional tableau id of a particular project
        '''
        assert project_id
        if not self.all_workbooks or kwargs.get("refresh_workbooks"):
            self.get_all_workbooks()
        
        e_project_workbooks = {}
        for workbook_id, o_wb in self.all_workbooks.items():
            if project_id == o_wb.project_id:
                e_project_workbooks[o_wb.id] = o_wb
        
        return e_project_workbooks
    
    def get_project_views(self, project_id, e_project_workbooks=None, **kwargs):
        ''' Get a particular project's views, refreshing if specified
            Arguments:
                project_id: optional tableau id of a particular project
        '''
        assert project_id
        if not e_project_workbooks:
            e_project_workbooks = self.get_project_workbooks(project_id)
        if not e_project_workbooks: return {}
        
        if not self.all_views or kwargs.get("refresh_views"):
            self.get_all_views()
        
        
        e_project_views = defaultdict(dict)
        for view_id, o_view in self.all_views.items():
            if o_view.workbook_id in e_project_workbooks:
                e_project_views[o_view.workbook_id][o_view.id] = o_view
        
        return e_project_views
    
    def get_whole_project(self, project_id, **kwargs):
        '''
            Get a project and all its items (datasources, flows, workbooks, views) and any subprojects
            Arguments:
                project_id: optional tableau id of a particular project
            Returns:
                {dict} of 
        '''
        if kwargs.get("refresh"):
            self.refresh()
        elif not self.all_projects or not self.all_projects.get(project_id):
            self.get_all_projects(**kwargs)
        
        if not self.all_projects.get(project_id): return None
        
        e_data_sources = self.get_project_datasources(project_id=project_id, **kwargs)
        e_flows = self.get_project_flows(project_id=project_id, **kwargs)
        
        e_workbooks = self.get_project_workbooks(project_id=project_id, **kwargs)
        e_views = self.get_project_views(project_id=project_id, e_project_workbooks=e_workbooks)
        
        a_child_project_ids = [
            sub_project_id
            for sub_project_id, o_sub_proj in self.all_projects.items()
            if project_id == o_sub_proj.parent_id]
        
        e_non_refresh_kwargs = {k: v for k, v in kwargs.items() if not k.startswith("refresh")}
        
        e_child_projects = {
            sub_project_id: self.get_whole_project(sub_project_id, **e_non_refresh_kwargs)
            for sub_project_id, o_sub_proj in self.all_projects.items()
            if project_id == o_sub_proj.parent_id}
        
        return {
            "project": self.all_projects[project_id],
            "data_sources": e_data_sources,
            "flows": e_flows,
            "workbooks": e_workbooks,
            "views": e_views,
            "child_project_ids": a_child_project_ids,
            "child_projects": e_child_projects,
        }


class ReplaceState:
    ''' class to replace a state/name or abbreviation using regex
    '''
    def __init__(self, s_old_name="", s_new_name="", s_old_abbr="", s_new_abbr=""):
        assert s_new_name or s_new_abbr
        
        self._old_name = s_old_name.strip()
        self._new_name = s_new_name.strip()
        self._old_abbr = s_old_abbr.strip()
        self._new_abbr = s_new_abbr.strip()
        
        #self._re_old_name = re.compile(r"(^|\b){}($|\b)".format(self._old_name), flags=re.I)
        #self._re_old_abbr = re.compile(r"(^|\b){}($|\b)".format(self._old_abbr), flags=re.I)
        self._re_old_name = re.compile(r"((^|\b){}|{}($|\b))".format(self._old_name, self._old_name), flags=re.I)
        self._re_old_abbr = re.compile(r"((^|\b){}|{}($|\b))".format(self._old_abbr, self._old_abbr), flags=re.I)
        
        if s_new_name: assert s_old_name
        if s_new_abbr: assert s_old_abbr
        
    def replace_or_insert(self, s_text, s_default=""):
        ''' Replace the old state name/abbreviations with the new ones,
            or append them if they don't exist to try to make the new string unique for tableau
            Arguments:
                s_text: {str} to replace values in
                s_default: {str} optional value if the old text is completely missing
            Returns:
                {str} of text with old state name/abbr replaced with new name/abbr, or new name/abbrev inserted
        '''
        if not s_text:
            if s_default:
                if self._new_abbr:
                    return f"{s_default} - {self._new_abbr}"
                elif self._new_name:
                    return f"{s_default} - {self._new_name}"
            else:
                return None
        
        b_replaced = False
        if self._new_name:
            if self._re_old_name.search(s_text): b_replaced = True
            s_text = self._re_old_name.sub(self._new_name, s_text)
        if self._new_abbr:
            if self._re_old_abbr.search(s_text): b_replaced = True
            s_text = self._re_old_abbr.sub(self._new_abbr, s_text)
        
        if not b_replaced:
            if self._new_abbr:
                s_text += f" - {self._new_abbr}"
            elif self._new_name:
                s_text += f" - {self._new_name}"
        
        return s_text
        
    def replace(self, s_text):
        ''' Replace the old state name/abbreviations with the new ones
            to try to make the new string unique for tableau
            Arguments:
                s_text: {str} to replace values in
            Returns:
                {str} of text with old state name/abbr replaced with new name/abbr
        '''
        if self._new_name:
            s_text = self._re_old_name.sub(self._new_name, s_text)
        if self._new_abbr:
            s_text = self._re_old_abbr.sub(self._new_abbr, s_text)
        return s_text


class TableauProjectDuper(TableauProject):
    ''' Download a project's configuration info, modify it, and then create a new project cloned from it '''
    
    _download_retries = 10
    _upload_retries = 10
    _proj_structure = {
        "project": None,
        "data_sources": {},
        "flows": {},
        "workbooks": {},
        "views": {},
        "child_project_ids": {},
        "child_projects": {},
    }
    
    def __init__(self, e_whole_project, e_old_new_maps, e_remaps=None, e_datasource_url_replacements=None, e_connection_credentials=None, **kwargs):
        '''
            Arguments:
                e_whole_project: {dict} of the whole project form TableauProject.get_whole_project()
                e_old_new_maps: {dict}
                    Prep values that will be substituted from the old objects to their new clones
                    For example, replacing an old server address to a connection or internal tableau id to replace in a link
                e_remaps: {dict}
                    ids that also need to be remapped once the new ids are known
                        {old_id1: [id_1_to_remap_to_old_id1s_replacement, id_2_to_remap_to_old_id1s_replacement...], ...}
        '''
        super().__init__(**kwargs)
        self._old = e_whole_project
        self._old_project = e_whole_project["project"]
        
        self._new = deepcopy(self._proj_structure)
        self._datasource_url_replacements = e_datasource_url_replacements if e_datasource_url_replacements else {}
        self._connection_credentials = e_connection_credentials if e_connection_credentials else {}
        
        self._old_new_maps = e_old_new_maps if isinstance(e_old_new_maps, dict) else {}
        self._remaps = e_remaps if isinstance(e_remaps, dict) else {}

        self._states = {k: kwargs[k] for k in ["s_old_name", "s_new_name", "s_old_abbr", "s_new_abbr"] if kwargs.get(k)}
        self._replacer = ReplaceState(**self._states)
        
        self._downloaded_files = defaultdict(dict)
    
    def add_remap(self, old_id, new_id, **kwargs):
        assert old_id in self._old_new_maps
        a_remaps = self._remaps.get(old_id, [])
        for id_to_remap in a_remaps:
            self._old_new_maps[id_to_remap] = new_id
    
    def duplicate_project(self, **kwargs):
        ''' Run the process of cloning all the item types '''
        self.duplicate_project_objects(self._old["project"], **kwargs)
        self.duplicate_project_datasources(self._old["project"], **kwargs)
        #self.duplicate_project_flows(self._old["project"])
        self.duplicate_project_workbooks(self._old["project"], **kwargs)
    
    def duplicate_project_objects(self, o_old_proj=None, e_parent_structure=None, **kwargs):
        ''' The project object and any sub-project objects
            Arguments:
                o_old_proj: optional the project object to clone if it isn't the parent project
        '''
        if o_old_proj is None: o_old_proj = self._old_project
        if e_parent_structure is None: e_parent_structure = self._new
        
        if o_old_proj.id not in self._old_new_maps:
            o_new_proj = self.dupe_project(o_old_proj)
            self._old_new_maps[o_old_proj.id] = o_new_proj.id
            self.add_remap(o_old_proj.id, o_new_proj.id)
            e_parent_structure["project"] = o_new_proj
        
    
        for s_old_sub_proj_id, e_old_sub_proj in self._old.get("child_projects", {}).items():
            o_old_sub_proj = e_old_sub_proj["project"]
            if s_old_sub_proj_id not in self._old_new_maps:
                o_new_sub_proj = self.dupe_project(o_old_sub_proj, **kwargs)
                self._old_new_maps[o_old_sub_proj.id] = o_new_sub_proj.id
                self.add_remap(o_old_sub_proj.id, o_new_sub_proj.id)
            
                if o_new_sub_proj.id not in self._new["child_projects"]:
                    self._new["child_projects"][o_new_sub_proj.id] = deepcopy(self._proj_structure)
                
                e_parent_structure["child_projects"][o_new_sub_proj.id]["project"] = o_new_sub_proj
            else:
                s_new_sub_proj_id = self._old_new_maps.get(s_old_sub_proj_id, s_old_sub_proj_id)
                assert s_new_sub_proj_id in e_parent_structure["child_projects"]
                o_new_sub_proj = e_parent_structure["child_projects"][s_new_sub_proj_id]["project"]
            
    
    def duplicate_project_datasources(self, e_proj=None, e_parent_structure=None, **kwargs):
        ''' The project's datasources and any sub-project's datasources
            Arguments:
                o_old_proj: optional the project object to clone from if it isn't the parent project
        '''
        if e_proj is None: e_proj = self._old
        if e_parent_structure is None: e_parent_structure = self._new
        
        for s_old_ds_id, o_old_ds in e_proj.get("data_sources", {}).items():
            o_new_ds = self.dupe_datasource(o_old_ds, **kwargs)
            if o_new_ds is not None:
                self._old_new_maps[o_old_ds.id] = o_new_ds.id
                self.add_remap(o_old_ds.id, o_new_ds.id)
                
                e_parent_structure["data_sources"][o_new_ds.id] = o_new_ds
        
        for s_old_proj_sub_id, e_old_sub_proj in e_proj.get("child_projects", {}).items():
            s_new_proj_sub_id = self._old_new_maps.get(s_old_proj_sub_id, s_old_proj_sub_id)
            self.duplicate_project_datasources(e_proj=e_old_sub_proj, e_parent_structure=e_parent_structure["child_projects"][s_new_proj_sub_id])
            
            #o_new_sub_proj = o_new_sub_ds.project_id
           # 
            #if o_new_sub_proj.id not in self._new["child_projects"]:
            #    self._new["child_projects"][o_new_sub_proj.id] = self._proj_structure.copy()
            #e_parent_structure["child_projects"][o_new_sub_proj.id]["data_sources"][o_new_sub_ds.id] = o_new_sub_ds
    
    def duplicate_project_flows(self, e_proj=None, e_parent_structure=None, **kwargs):
        ''' The project's flows and any sub-project's flows
            Arguments:
                o_old_proj: optional the project object to clone from if it isn't the parent project
        '''
        if e_proj is None: e_proj = self._old
        if e_parent_structure is None: e_parent_structure = self._new
        
        for s_old_flow_id, o_old_flow in e_proj.get("flows", {}).items():
            o_new_flow = self.dupe_flow(o_old_flow, **kwargs)
            if o_new_flow is not None:
                self._old_new_maps[o_old_flow.id] = o_new_flow.id
                self.add_remap(o_old_flow.id, o_new_flow.id)
                e_parent_structure["flows"][o_new_flow.id] = o_new_flow
        
        for s_old_proj_sub_id, e_old_sub_proj in e_proj.get("child_projects", {}).items():
            s_new_proj_sub_id = self._old_new_maps.get(s_old_proj_sub_id, s_old_proj_sub_id)
            self.duplicate_project_flows(e_proj=e_old_sub_proj, e_parent_structure=e_parent_structure["child_projects"][s_new_proj_sub_id])
    
    def duplicate_project_workbooks(self, e_proj=None, e_parent_structure=None, **kwargs):
        ''' The project's workbooks and any sub-project's workbooks
            Arguments:
                o_old_proj: optional the project object to clone from if it isn't the parent project
        '''
        if e_proj is None: e_proj = self._old
        if e_parent_structure is None: e_parent_structure = self._new
        
        for s_old_wb_id, o_old_book in e_proj.get("workbooks", {}).items():
            o_new_book = self.dupe_workbook(o_old_book, **kwargs)
            if o_new_book is not None:
                self._old_new_maps[o_old_book.id] = o_new_book.id
                self.add_remap(o_old_book.id, o_new_book.id)
                e_parent_structure["workbooks"][o_new_book.id] = o_new_book
        
        for s_old_proj_sub_id, e_old_sub_proj in e_proj.get("child_projects", {}).items():
            s_new_proj_sub_id = self._old_new_maps.get(s_old_proj_sub_id, s_old_proj_sub_id)
            self.duplicate_project_workbooks(e_proj=e_old_sub_proj, e_parent_structure=e_parent_structure["child_projects"][s_new_proj_sub_id])
    
    ##### Cloning individual objects #####
    def dupe_project(self, o_old_proj, **kwargs):
        ''' Clone a project object, replacing old state name/abbrevation and any changed ids from the old project
            Arguments:
                o_old_proj: the project object to clone
            Returns:
                the new cloned project
        '''
        
        if o_old_proj is None: return None
        
        if o_old_proj.name:
            self._old_new_maps[o_old_proj.name] = self._replacer.replace_or_insert(o_old_proj.name, "Project")
        
        o_new_proj_item = TSC.ProjectItem(
            name=self._replacer.replace_or_insert(o_old_proj.name, "Project"),
            content_permissions=o_old_proj.content_permissions,
            description=self._replacer.replace_or_insert(o_old_proj.description),
            parent_id=self._old_new_maps.get(o_old_proj.parent_id, o_old_proj.parent_id),
        )
        
        o_new_proj_item._permissions = o_new_proj_item._permissions
        o_new_proj_item._default_workbook_permissions = o_new_proj_item._default_workbook_permissions
        o_new_proj_item._default_datasource_permissions = o_new_proj_item._default_datasource_permissions
        o_new_proj_item._default_flow_permissions = o_new_proj_item._default_flow_permissions
        
        o_new_project = self._server.projects.create(o_new_proj_item)
        
        self._server.projects.populate_permissions(o_new_project)
        self._server.projects.populate_workbook_default_permissions(o_new_project)
        self._server.projects.populate_datasource_default_permissions(o_new_project)
        self._server.projects.populate_flow_default_permissions(o_new_project)
        
        return o_new_project
    
    def dupe_connection(self, o_old_conn, **kwargs):
        ''' Clone a connection object, replacing old state name/abbrevation, server address
            Arguments:
                o_old_conn: the connection object to clone
            Returns:
                the new cloned connection object
        '''
        if o_old_conn is None: return None
        o_new_conn_item = TSC.ConnectionItem()
        
        if o_old_conn._datasource_name and not self._old_new_maps.get(o_old_conn._datasource_name):
            self._old_new_maps[o_old_conn._datasource_name] = self._replacer.replace_or_insert(o_old_conn._datasource_name, "Connection")
        
        o_new_conn_item._datasource_id = self._old_new_maps.get(o_old_conn._datasource_id, o_old_conn._datasource_id)
        o_new_conn_item._datasource_name = self._replacer.replace_or_insert(o_old_conn._datasource_name, "Connection")
        #o_new_conn_item._id = o_old_conn._id
        o_new_conn_item._connection_type = o_old_conn._connection_type
        
        o_new_conn_item.server_address = self._old_new_maps.get(o_old_conn.server_address, o_old_conn.server_address)
        
        e_creds = self._connection_credentials.get(o_new_conn_item.server_address, {})
        
        s_password = self._old_new_maps.get(o_old_conn.password, o_old_conn.password) if not e_creds.get("password") else e_creds["password"]
        s_username = self._old_new_maps.get(o_old_conn.username, o_old_conn.username) if not e_creds.get("username") else e_creds["username"]
        #print(o_new_conn_item.server_address, s_password, s_username)
        o_new_conn_item.server_port = self._old_new_maps.get(o_old_conn.server_port, o_old_conn.server_port) if not e_creds.get("port") else e_creds["port"]
        
        #o_new_conn_item.connection_credentials = o_old_conn.connection_credentials
        o_new_conn_item.embed_password = True if s_password else o_old_conn.embed_password
        o_new_conn_item.username = s_username
        o_new_conn_item.password = s_password
        o_new_cred = TSC.ConnectionCredentials(name=s_username, password=s_password, embed=True, oauth=False)
        assert o_new_cred.oauth is not None
        o_new_conn_item.connection_credentials = o_new_cred
        
        #print(f"""old_id:{o_old_conn.id}, ds_name:{o_new_conn_item._datasource_name}, user:{o_new_conn_item.username}, pass:{o_new_conn_item.password}, url:{o_new_conn_item.server_address}""")
        
        return o_new_conn_item
    
    def dupe_flow(self, o_old_flow, **kwargs):
        ''' Clone a flow object, replacing old state name/abbrevation
            
THERE IS PROBLEM DOWNLOADING FLOW CONFIGURATION FILES FROM TABLEAU API < 3.3
            
            Arguments:
                o_old_flow: the flow object to clone
            Returns:
                the new cloned flow object
        '''
        if o_old_flow is None: return None
        if o_old_flow.name and not self._old_new_maps.get(o_old_flow.name):
            self._old_new_maps[o_old_flow.name] = self._replacer.replace_or_insert(o_old_flow.name, "Flow")
        
        o_new_flow_item = TSC.FlowItem(
            project_id=self._old_new_maps.get(o_old_flow.project_id, o_old_flow.project_id),
            name=self._replacer.replace_or_insert(o_old_flow.name, "Flow"),
        )
        
        o_new_flow_item.owner_id = o_old_flow.owner_id
        if o_old_flow.tags: o_new_flow_item.tags = o_old_flow.tags
        o_new_flow_item.description = self._replacer.replace_or_insert(o_old_flow.description)
        
        a_new_connections = []
        for o_old_conn in o_old_flow.connections:
            o_new_conn = self.dupe_connection(o_old_conn)
            if o_new_conn: a_new_connections.append(o_new_conn)
        if a_new_connections:
            o_new_flow_item._set_connections(a_new_connections)
        
        o_new_flow_item._set_permissions(o_old_flow._permissions)
        utils.create_folder(os.path.join(s_TMP, "flows", o_old_flow.id, ""))
        s_downloaded_file = ""
        if os.path.isfile(os.path.join(s_TMP, "flows", o_old_flow.id, f"{o_old_flow.id}.tflx")):
            s_downloaded_file = os.path.join(s_TMP, "flows", o_old_flow.id, f"{o_old_flow.id}.tflx")
        else:
            for i in range(0, self._download_retries):
                try:
                    s_downloaded_file = self._server.flows.download(o_old_flow.id, os.path.join(s_TMP, "flows", o_old_flow.id, f"{o_old_flow.id}"))
                    if s_downloaded_file and os.path.isfile(s_downloaded_file):
                        self._downloaded_files["flows"][o_old_flow.id] = s_downloaded_file
                        break
                except:
                    print(f"Flow download retry: {i + 1}")
                    sleep(1)
        
        print("downloaded flow file:", s_downloaded_file)
        
        a_flow_files = unzip(s_downloaded_file, temp=os.path.split(s_downloaded_file)[0])
        s_new_file = ""
        for s_file in a_flow_files:
            folder, filename = os.path.split(s_file)
            if filename.lower() in {"flow", "flow.tfl", "flow.tflx"}:
                e_json = json.load(open(s_file, "r"))
                
                c_uid_paths = {
                    ("initialNodes",),
                    ("nodes",),
                    ("nodes", "id"),
                    ("nodes", "nextNodes"),
                    ("nodes", "nextNodes", "nextNodeId"),
                    ("nodes", "connectionId"),
                    ("nodes", "projectLuid"),
                    ("nodes", "beforeActionAnnotations"),
                    ("nodes", "beforeActionAnnotations", "annotationNode"),
                    ("nodes", "beforeActionAnnotations", "annotationNode", "id"),
                    ("documentId",),
                    ("obfuscatorId",),
                    ("connections",),
                    ("connectionIds",),
                }

                e_new_json = remove_null(replace_values(e_json, self._old_new_maps, c_uid_paths=c_uid_paths))
                #s_flow_file = os.path.join(folder, "new_flow.tfl")
                json.dump(e_new_json, open(s_file, "w"), indent=2)
                
        for s_file in a_flow_files:
            folder, filename = os.path.split(s_file)
            if filename.lower() in {"displaysettings", "displaysetting", "displaysettings.json"}:
                e_display_settings = json.load(open(s_file, "r"))
                e_new_json = remove_null(replace_values(e_display_settings, self._old_new_maps))
                #s_flow_file = os.path.join(folder, "new_flow.tfl")
                json.dump(e_new_json, open(s_file, "w"), indent=2)
        
        s_new_file = os.path.join(s_TMP, "flows", o_old_flow.id, f"{o_old_flow.id}_new.tflx")
        o_zip = zip(s_new_file, a_flow_files)
        
        o_new_flow = None
        if s_new_file:
            #for i in range(0, self._upload_retries):
                #try:
            o_new_flow = self._server.flows.publish(o_new_flow_item, s_new_file, TSC.Server.PublishMode.CreateNew) #, connections=a_new_connections)
                    #break
                #except:
                #    print(f"Flow upload retry: {i + 1}")
                #    sleep(1)
            self._server.flows.populate_connections(o_new_flow)
            self._server.flows.populate_dqw(o_new_flow)
            self._server.flows.populate_permissions(o_new_flow)
            
            for o_conn in o_new_flow.connections:
                #print(o_conn.id, o_conn.server_address, o_conn.username, o_conn.password)
            
                e_creds = self._connection_credentials.get(o_conn.server_address, {})
                if e_creds.get("username") and e_creds.get("password"):
                    #print(o_new_flow.id, o_conn.id, o_conn.server_address, o_conn.username, o_conn.password, e_creds.get("username"), e_creds.get("password"))
                    
                    o_conn.embed_password = True
                    o_conn.username = e_creds["username"]
                    o_conn.password = e_creds["password"]
                    o_new_cred = TSC.ConnectionCredentials(name=e_creds["username"], password=e_creds["password"], embed=True, oauth=False)
                    assert o_new_cred.oauth is not None
                    o_conn.connection_credentials = o_new_cred
                    
                    self._server.flows.update_connection(o_new_flow, o_conn)
                
            return o_new_flow
        else:
            return None
    
    def dupe_datasource(self, o_old_ds):
        ''' Clone a datasource object, replacing old state name/abbrevation
            
            Arguments:
                o_old_ds: the datasource object to clone
            Returns:
                the new cloned datasource object
        '''
        if o_old_ds is None: return None
        
        if o_old_ds.name and not self._old_new_maps.get(o_old_ds.name):
            self._old_new_maps[o_old_ds.name] = self._replacer.replace_or_insert(o_old_ds.name, "Datasource")
        
        o_new_ds_item = TSC.DatasourceItem(
            project_id=self._old_new_maps.get(o_old_ds.project_id, o_old_ds.project_id),
            name=self._replacer.replace_or_insert(o_old_ds.name, "Datasource"),
        )
        
        if o_old_ds._content_url and not self._old_new_maps.get(o_old_ds._content_url):
            self._old_new_maps[o_old_ds._content_url] = re.sub("[- ]+", "", self._replacer.replace_or_insert(o_old_ds._content_url))
        self._datasource_url_replacements[o_old_ds._content_url] = re.sub("[- ]+", "", self._replacer.replace_or_insert(o_old_ds._content_url))
        
        #if o_old_ds._webpage_url and not self._old_new_maps.get(o_old_ds._webpage_url):
        #    self._old_new_maps[o_old_ds._webpage_url] = re.sub("[- ]+", "", self._replacer.replace_or_insert(o_old_ds._webpage_url))
        #self._datasource_url_replacements[o_old_ds._webpage_url] = re.sub("[- ]+", "", self._replacer.replace_or_insert(o_old_ds._webpage_url))
        
        o_new_ds_item._set_values(
            o_old_ds._ask_data_enablement,
            o_old_ds._certified,
            o_old_ds._certification_note,
            re.sub("[- ]+", "", self._replacer.replace_or_insert(o_old_ds._content_url)),
            None, #created_at,
            o_old_ds._datasource_type,
            self._replacer.replace_or_insert(o_old_ds._description),
            o_old_ds._encrypt_extracts,
            o_old_ds._has_extracts,
            None, #id_,
            o_new_ds_item.name,
            o_old_ds.owner_id,
            None, #project_id,
            None, #project_name,
            o_old_ds.tags,
            None, #updated_at,
            o_old_ds._use_remote_query_agent,
            None, #re.sub("[- ]+", "", self._replacer.replace_or_insert(o_old_ds._webpage_url)),
        )
        
        a_new_connections = []
        e_old_conn_address_ids = {}
        e_old_conn_index_ids = {}
        for idx, o_old_conn in enumerate(o_old_ds.connections):
            e_old_conn_address_ids[o_old_conn.server_address] = o_old_conn.id
            e_old_conn_index_ids[idx] = o_old_conn.id
            o_new_conn = self.dupe_connection(o_old_conn)
            
            if o_new_conn: a_new_connections.append(o_new_conn)
        if a_new_connections:
            o_new_ds_item._set_connections(a_new_connections)
        
        o_new_ds_item._set_permissions(o_old_ds._permissions)
        
        utils.create_folder(os.path.join(s_TMP, "datasources", o_old_ds.id, ""))
        #s_file = self._server.datasources.download(o_old_ds.id, os.path.join(s_TMP, "datasources", o_old_ds.id, f"{o_old_ds.id}"), include_extract=False)
        if os.path.isfile(os.path.join(s_TMP, "datasources", o_old_ds.id, f"{o_old_ds.id}.tdsx")):
            s_downloaded_file = os.path.join(s_TMP, "datasources", o_old_ds.id, f"{o_old_ds.id}.tdsx")
            self._downloaded_files["datasources"][o_old_ds.id] = s_downloaded_file
        else:
            s_downloaded_file = ""
            for i in range(0, self._download_retries):
                try:
                    s_downloaded_file = self._server.datasources.download(o_old_ds.id, os.path.join(s_TMP, "datasources", o_old_ds.id, f"{o_old_ds.id}"), include_extract=False)
                    if s_downloaded_file and os.path.isfile(s_downloaded_file):
                        self._downloaded_files["datasources"][o_old_ds.id] = s_downloaded_file
                        break
                except:
                    print(f"Datasource download retry: {i + 1}")
                    sleep(1)
        
        print("datasource file:", s_downloaded_file)
        print("a_new_connections:", a_new_connections)
        
        a_tds_files = unzip(s_downloaded_file, temp=os.path.split(s_downloaded_file)[0])
        
        s_old_extract = a_tds_files[0]
        print("s_old_extract", s_old_extract)
        
        replace_xml_values(s_old_extract, s_old_extract, self._old_new_maps, o_replacer=self._replacer)
        
        s_new_file = os.path.join(s_TMP, "datasources", o_old_ds.id, f"{o_old_ds.id}_new.tdsx")
        o_zip = zip(s_new_file, a_tds_files)
        
        o_new_datasource = self._server.datasources.publish(o_new_ds_item, s_old_extract, TSC.Server.PublishMode.CreateNew) #, connections=a_new_connections)
        
        self._server.datasources.populate_connections(o_new_datasource)
        self._server.datasources.populate_dqw(o_new_datasource)
        self._server.datasources.populate_permissions(o_new_datasource)
        
        for o_conn in o_new_datasource.connections:
            #print(o_conn.id, o_conn.server_address, o_conn.username, o_conn.password)
        
            e_creds = self._connection_credentials.get(o_conn.server_address, {})
            if e_creds.get("username") and e_creds.get("password"):
                #print(o_new_datasource.id, o_conn.id, o_conn.server_address, o_conn.username, o_conn.password, e_creds.get("username"), e_creds.get("password"))
                
                o_conn.embed_password = True
                o_conn.username = e_creds["username"]
                o_conn.password = e_creds["password"]
                o_new_cred = TSC.ConnectionCredentials(name=e_creds["username"], password=e_creds["password"], embed=True, oauth=False)
                assert o_new_cred.oauth is not None
                o_conn.connection_credentials = o_new_cred
                
                self._server.datasources.update_connection(o_new_datasource, o_conn)
        
        if o_old_ds.webpage_url:
            self._datasource_url_replacements[o_old_ds.webpage_url] = o_new_datasource.webpage_url
            self._old_new_maps[o_old_ds.webpage_url] = o_new_datasource.webpage_url
            self._old_new_maps[get_url_base(o_old_ds.webpage_url)] = get_url_base(o_new_datasource.webpage_url)
            #self._datasource_url_replacements[get_url_base(o_old_ds.webpage_url)] = get_url_base(o_new_datasource.webpage_url)
        if o_old_ds.name:
            self._old_new_maps[o_old_ds.name] = o_new_datasource.name
            self._datasource_url_replacements[o_old_ds.name] = o_new_datasource.name
            
            s_old_clean = re_URL_SAFE.sub("", o_old_ds.name)
            s_new_clean = re_URL_SAFE.sub("", o_new_datasource.name)
            if s_old_clean and s_old_clean:
                if s_old_clean not in self._old_new_maps:
                    self._old_new_maps[s_old_clean] = s_new_clean
                if s_old_clean not in self._datasource_url_replacements:
                    self._datasource_url_replacements[s_old_clean] = s_new_clean
        
        if o_old_ds.content_url:
            self._old_new_maps[o_old_ds.content_url] = o_new_datasource.content_url
            self._datasource_url_replacements[o_old_ds.content_url] = o_new_datasource.content_url
        
        for idx, o_new_conn in enumerate(o_new_datasource.connections):
            s_new_address, s_new_id = o_new_conn.server_address, o_new_conn.id
            if s_new_address and e_old_conn_address_ids.get(s_new_address):
                print("map old connection id to new connection id", s_new_address, e_old_conn_address_ids[s_new_address], s_new_id)
                self._old_new_maps[e_old_conn_address_ids[s_new_address]] = s_new_id
                
                self.add_remap(e_old_conn_address_ids[s_new_address], s_new_id)
            
            if e_old_conn_index_ids.get(idx):
                print("map old connection id to new connection id", idx, e_old_conn_index_ids[idx], s_new_id)
                self._old_new_maps[e_old_conn_index_ids[idx]] = s_new_id
                self.add_remap(e_old_conn_index_ids[idx], s_new_id)
        
        return o_new_datasource

    
    def dupe_view(self, o_old_view, **kwargs):
        ''' Clone a view object, replacing old state name/abbrevation
            
            Arguments:
                o_old_ds: the view object to clone
            Returns:
                the new cloned view object
        '''
        if o_old_view is None: return None
        if o_old_view._name and not self._old_new_maps.get(o_old_view._name):
            self._old_new_maps[o_old_view._name] = self._replacer.replace_or_insert(o_old_view._name)
        
        o_new_view_item = TSC.ViewItem()
        
        o_new_view_item._content_url = self._old_new_maps.get(o_old_view._content_url, o_old_view._content_url)
        o_new_view_item._created_at = o_old_view._created_at
        #o_new_view_item._id = o_old_view._id
        o_new_view_item._image = o_old_view._image
        #o_new_view_item._initial_tags = set()
        o_new_view_item._name = self._replacer.replace_or_insert(o_old_view._name)
        o_new_view_item._owner_id = self._old_new_maps.get(o_old_view._owner_id, o_old_view._owner_id)
        o_new_view_item._preview_image = o_old_view._preview_image
        o_new_view_item._project_id = self._old_new_maps.get(o_old_view._project_id, o_old_view._project_id)
        #o_new_view_item._pdf = o_old_view._pdf
        #o_new_view_item._csv = o_old_view._csv
        #o_new_view_item._total_views = o_old_view._total_views
        o_new_view_item._sheet_type = o_old_view._sheet_type
        #o_new_view_item._updated_at = o_old_view._updated_at
        o_new_view_item._workbook_id = kwargs.get("workbook_id", self._old_new_maps.get(o_old_view._workbook_id, o_old_view._workbook_id))
        o_new_view_item._permissions = o_old_view._permissions
        o_new_view_item.tags = o_old_view.tags
        
        return o_new_view_item
    
    def dupe_workbook(self, o_old_book, **kwargs):
        ''' Clone a workbook object, replacing old state name/abbrevation
            
            Arguments:
                o_old_book: the workbook object to clone
            Returns:
                the new cloned workbook object
        '''
        if o_old_book is None: return None
        
        if o_old_book._project_name:
            self._old_new_maps[o_old_book._project_name] = self._replacer.replace_or_insert(o_old_book._project_name)
        if o_old_book.name:
            self._old_new_maps[o_old_book.name] = self._replacer.replace_or_insert(o_old_book.name, "Workbook")
        
        s_old_clean = re_URL_SAFE.sub("", o_old_book.name)
        s_new_clean = re_URL_SAFE.sub("", self._replacer.replace_or_insert(o_old_book.name, "Workbook"))
        if s_old_clean and s_old_clean:
            if s_old_clean not in self._old_new_maps:
                self._old_new_maps[s_old_clean] = s_new_clean
            if s_old_clean not in self._datasource_url_replacements:
                self._datasource_url_replacements[s_old_clean] = s_new_clean
        
        o_new_book_item = TSC.WorkbookItem(
            project_id=self._old_new_maps.get(o_old_book.project_id, o_old_book.project_id),
            name=self._replacer.replace_or_insert(o_old_book.name, "Workbook"),
        )
        
        
        o_new_book_item._set_values(
            id=None,
            name=None, #self._replacer.replace_or_insert(o_old_book.name, "Workbook"),
            content_url=self._old_new_maps.get(o_old_book._content_url),
            webpage_url=self._old_new_maps.get(o_old_book._webpage_url),
            created_at=None, #o_old_book._created_at,
            description=self._replacer.replace_or_insert(o_old_book._description),
            updated_at=None, #o_old_book._updated_at,
            size=None, #o_old_book._size,
            show_tabs=o_old_book._show_tabs,
            project_id=None, #o_old_book.project_id,
            project_name=self._replacer.replace_or_insert(o_old_book._project_name),
            owner_id=o_old_book.owner_id,
            tags=o_old_book.tags,
            views=o_old_book._views,
            data_acceleration_config=o_old_book.data_acceleration_config,
        )
        
        o_new_book_item._set_permissions(o_old_book._permissions)
        
        
        a_new_views = []
        #e_old_conn_address_ids = {}
        #e_old_conn_index_ids = {}
        for o_old_view in o_old_book.views:
            #e_old_conn_address_ids[o_old_conn.server_address] = o_old_conn.id
            o_new_view = self.dupe_view(o_old_view)
            if o_new_view: a_new_views.append(o_new_view)
        if a_new_views:
            #o_new_ds_item._set_connections(a_new_connections)
            o_new_book_item._views = a_new_views
        
        utils.create_folder(os.path.join(s_TMP, "workbooks", o_old_book.id, ""))
        #s_file = self._server.workbooks.download(o_old_book.id, os.path.join(s_TMP, "workbooks", o_old_book.id, f"{o_old_book.id}"))
        if os.path.isfile(os.path.join(s_TMP, "workbooks", o_old_book.id, f"{o_old_book.id}.twbx")):
            s_downloaded_file = os.path.join(s_TMP, "workbooks", o_old_book.id, f"{o_old_book.id}.twbx")
        else:
        
            s_downloaded_file = ""
            for i in range(0, self._download_retries):
                try:
                    s_downloaded_file = self._server.workbooks.download(o_old_book.id, os.path.join(s_TMP, "workbooks", o_old_book.id, f"{o_old_book.id}"))
                    if s_downloaded_file and os.path.isfile(s_downloaded_file):
                        self._downloaded_files["workbooks"][o_old_book.id] = s_downloaded_file
                        break
                except:
                    print(f"Workbook download retry: {i + 1}")
                    sleep(1)
            
        print("workbook file:", s_downloaded_file)
        
        s_new_wb = ""
        
        #a_book_files = unzip(s_downloaded_file, temp=os.path.split(s_downloaded_file)[0])
        if not kwargs.get("skip_update"):
            if s_downloaded_file.lower().endswith(".twbx"):
                a_files = unzip(s_downloaded_file, temp=os.path.split(s_downloaded_file)[0])
                a_twb_files = [s_file for s_file in a_files if s_file.lower().endswith(".twb")]
                if a_twb_files:
                    s_twb_file = a_twb_files[0]
                else:
                    s_twb_file = ""
            else:
                s_twb_file = s_downloaded_file
            
            if s_twb_file:
                s_new_wb = os.path.join(s_TMP, "workbooks", o_old_book.id, f"{o_old_book.id}_new.twb")
                a_replace_regexes = [
                    (re.compile(r"\b{}\b".format(re.escape(t[0])), flags=re.I), t[1])
                    for t in sorted(self._datasource_url_replacements.items(), key=lambda t: len(t[0]), reverse=True)]
                replace_xml_values(s_twb_file, s_new_wb, self._old_new_maps, a_regexes=a_replace_regexes, o_replacer=self._replacer)
        else:
            s_new_wb = s_downloaded_file
            
        if s_new_wb:
            o_new_wb = self._server.workbooks.publish(o_new_book_item, s_new_wb, TSC.Server.PublishMode.CreateNew) #, connections=a_new_connections)
            return o_new_wb
        else:
            return None
    
    
    def make_flow_request_file(self, o_old_flow, s_filename=""):
        flow_name = self._replacer.replace_or_insert(o_old_flow.name)
        if o_old_flow.name:
            self._old_new_maps[o_old_flow.name] = flow_name
        
        project_id = self._old_new_maps.get(o_old_flow.project_id, o_old_flow.project_id)
        
        a_connections = []
        for o_old_conn in o_old_flow.connections[:1]:
            connection_name = self._old_new_maps.get(o_old_conn.server_address, o_old_conn.server_address)
            connection_id = self._old_new_maps.get(o_old_conn.id, o_old_conn.id)
            connection_address = o_old_conn.server_address if o_old_conn.server_address else ""
            connection_port = o_old_conn.server_port if o_old_conn.server_port else ""
            connection_username = o_old_conn.username if o_old_conn.username else ""
            connection_password = o_old_conn.password if o_old_conn.password else ""
            embed = 'embed="embed-flag"' if o_old_conn.embed_password else ""
            
            s_connection = f"""
  <connection id="{connection_id}"
    name="{connection_name}"
    server-address="{connection_address}"
    port-number="{connection_port}"
    password="{connection_password}"
    username="{connection_username}"
    {embed}>
  <connectionCredentials
    name="{connection_username}"
    password="{connection_password}"
    {embed}/>
  </connection>"""
            a_connections.append(s_connection)
            
        s_all_connections = "\n".join(a_connections)
        s_request = f"""
<?xml version='1.0' encoding='utf-8'?>
<tsRequest>
  <flow name="{flow_name}" >
    <project id="{project_id}" />
  </flow>
  <connections>
{s_all_connections}
  </connections>
</tsRequest>"""
        if s_filename:
            with open(s_filename, "w", encoding="utf-8") as f:
                f.write(s_request)
        return s_request


def find_old_new_maps(
    e_source,
    e_cloned,
    e_old_new_maps,
    o_replacer,
    e_cloned_object_by_type=None,
    e_maps_by_type=None,
    b_flows=True,
    b_datasources=True,
    b_workbooks=True,
    b_views=True,
    b_connections=True
):
    if e_cloned_object_by_type is None: e_cloned_object_by_type = defaultdict(list)
    if e_maps_by_type is None: e_maps_by_type = defaultdict(dict)
    
    e_cloned_project_names = {
        e_proj["project"].name: s_proj_id
        for s_proj_id, e_proj in e_cloned["child_projects"].items()}

    for s_proj_id, e_proj in e_source["child_projects"].items():
        o_proj = e_proj["project"]
        s_source_name = e_proj["project"].name
        s_replaced_name = o_replacer.replace_or_insert(s_source_name)
        s_new_id = ""
        
        if s_replaced_name in e_cloned_project_names:
            s_new_id = e_cloned_project_names[s_replaced_name]
        elif s_source_name in e_cloned_project_names:
            s_new_id = e_cloned_project_names[s_source_name]
            
        if s_new_id:
            e_old_new_maps[s_proj_id] = s_new_id
            e_maps_by_type["projects"][s_proj_id] = s_new_id
            #if "projects" not in e_cloned_object_by_type: e_cloned_object_by_type["projects"] = []
            e_cloned_object_by_type["projects"].append(s_new_id)
            
            o_new_proj = e_cloned["child_projects"][s_new_id]["project"]
            print(f"Map old project '{o_proj.name}' to new project '{o_new_proj.name}")
    
    if b_flows:
        e_cloned_flow_names = {
            o_flow.name: s_flow_id
            for s_flow_id, o_flow in e_cloned["flows"].items()}
        
        for s_flow_id, o_flow in e_source["flows"].items():
            s_source_name = o_flow.name
            s_replaced_name = o_replacer.replace_or_insert(s_source_name)
            s_new_id = ""
            
            if s_replaced_name in e_cloned_flow_names:
                s_new_id = e_cloned_flow_names[s_replaced_name]
            elif s_source_name in e_cloned_flow_names:
                s_new_id = e_cloned_flow_names[s_source_name]
            
            if s_new_id:
                e_old_new_maps[s_flow_id] = s_new_id
                e_maps_by_type["flows"][s_flow_id] = s_new_id
                #if "flows" not in e_cloned_object_by_type: e_cloned_object_by_type["flows"] = []
                e_cloned_object_by_type["flows"].append(s_new_id)
            
                o_new_flow = e_cloned["flows"][s_new_id]
                print(f"Map old flow '{o_flow.name}' to new flow '{o_new_flow.name}")
                
                if b_connections:
                    #if "flow_connections" not in e_cloned_object_by_type: e_cloned_object_by_type["flow_connections"] = []
                    e_old_conn_address_ids = {
                        e_old_new_maps.get(o_conn.server_address, o_conn.server_address): o_conn.id
                        for o_conn in o_flow.connections}
                    
                    #print(b_connections, o_flow.name, "e_old_conn_address_ids", e_old_conn_address_ids)
                    for o_conn in o_new_flow.connections:
                        if o_conn.server_address in e_old_conn_address_ids:
                            e_old_new_maps[e_old_conn_address_ids[o_conn.server_address]] = o_conn.id
                            e_maps_by_type["flow_connections"][e_old_conn_address_ids[o_conn.server_address]] = o_conn.id
                            e_cloned_object_by_type["flow_connections"].append(o_conn.id)

    if b_datasources:
        e_cloned_ds_names = {
            o_ds.name: s_ds_id
            for s_ds_id, o_ds in e_cloned["data_sources"].items()}
        
        for s_ds_id, o_ds in e_source["data_sources"].items():
            s_source_name = o_ds.name
            s_replaced_name = o_replacer.replace_or_insert(s_source_name)
            s_new_id = ""
            
            if s_replaced_name in e_cloned_ds_names:
                s_new_id = e_cloned_ds_names[s_replaced_name]
            elif s_source_name in e_cloned_ds_names:
                s_new_id = e_cloned_ds_names[s_source_name]
            
            if s_new_id:
                e_old_new_maps[s_ds_id] = s_new_id
                e_maps_by_type["datasources"][s_ds_id] = s_new_id
                #if "datasources" not in e_cloned_object_by_type: e_cloned_object_by_type["datasources"] = []
                e_cloned_object_by_type["datasources"].append(s_new_id)
                
                o_new_ds = e_cloned["data_sources"][s_new_id]
                print(f"Map old datasource '{o_ds.name}' to new datasource '{o_new_ds.name}")
                
                if b_connections:
                    #if "datasource_connections" not in e_cloned_object_by_type: e_cloned_object_by_type["datasource_connections"] = []
                    e_old_conn_address_ids = {
                        e_old_new_maps.get(o_conn.server_address, o_conn.server_address): o_conn.id
                        for o_conn in o_ds.connections}
                    #print(b_connections, o_ds.name, "e_old_conn_address_ids", e_old_conn_address_ids)
                    for o_conn in o_new_ds.connections:
                        if o_conn.server_address in e_old_conn_address_ids:
                            e_old_new_maps[e_old_conn_address_ids[o_conn.server_address]] = o_conn.id
                            e_maps_by_type["datasource_connections"][e_old_conn_address_ids[o_conn.server_address]] = o_conn.id
                            e_cloned_object_by_type["datasource_connections"].append(o_conn.id)

    if b_workbooks:
        e_cloned_wb_names = {
            o_wb.name: s_wb_id
            for s_wb_id, o_wb in e_cloned["workbooks"].items()}
        
        for s_wb_id, o_wb in e_source["workbooks"].items():
            s_source_name = o_wb.name
            s_replaced_name = o_replacer.replace_or_insert(s_source_name)
            s_new_id = ""
            #print("s_source_name", s_source_name, " - s_replaced_name", s_replaced_name)
            if s_replaced_name in e_cloned_wb_names:
                s_new_id = e_cloned_wb_names[s_replaced_name]
            elif s_source_name in e_cloned_wb_names:
                s_new_id = e_cloned_wb_names[s_source_name]
            
            if s_new_id:
                e_old_new_maps[s_wb_id] = s_new_id
                e_maps_by_type["workbooks"][s_wb_id] = s_new_id
                #if "workbooks" not in e_cloned_object_by_type: e_cloned_object_by_type["workbooks"] = []
                e_cloned_object_by_type["workbooks"].append(s_new_id)
                
                o_new_wb = e_cloned["workbooks"][s_new_id]
                print(f"Map old workbook '{o_wb.name}' to new workbook '{o_new_wb.name}")
                
                if b_connections:
                    #if "workbook_connections" not in e_cloned_object_by_type: e_cloned_object_by_type["workbook_connections"] = []
                    e_old_conn_address_ids = {
                        e_old_new_maps.get(o_conn.server_address, o_conn.server_address): o_conn.id
                        for o_conn in o_wb.connections}
                    
                    #print(b_connections, o_wb.name, "e_old_conn_address_ids", e_old_conn_address_ids)
                    for o_conn in o_new_wb.connections:
                        if o_conn.server_address in e_old_conn_address_ids:
                            e_old_new_maps[e_old_conn_address_ids[o_conn.server_address]] = o_conn.id
                            e_maps_by_type["workbook_connections"][e_old_conn_address_ids[o_conn.server_address]] = o_conn.id
                            e_cloned_object_by_type["workbook_connections"].append(o_conn.id)
    
    if b_views:
        e_cloned_workbook_view_names = {
            s_wb_id: {
                o_vw.name: s_vw_id
                for s_vw_id, o_vw in e_views.items()}
            for s_wb_id, e_views in e_cloned["views"].items()}
        
        for s_wb_id, e_views in e_source["views"].items():
            s_new_wb_id = e_old_new_maps.get(s_wb_id, "")
            if not s_new_wb_id: continue
            
            e_cloned_vw_names = e_cloned_workbook_view_names.get(s_new_wb_id, {})
            
            for s_vw_id, o_vw in e_views.items():
                s_source_name = o_vw.name
                s_replaced_name = o_replacer.replace_or_insert(s_source_name)
                s_new_id = ""
                
                if s_replaced_name in e_cloned_vw_names:
                    s_new_id = e_cloned_vw_names[s_replaced_name]
                elif s_source_name in e_cloned_vw_names:
                    s_new_id = e_cloned_vw_names[s_source_name]
                
                if s_new_id:
                    e_old_new_maps[s_vw_id] = s_new_id
                    e_maps_by_type["views"][s_wb_id] = s_new_id
                    #if "views" not in e_cloned_object_by_type: e_cloned_object_by_type["views"] = []
                    e_cloned_object_by_type["views"].append(s_new_id)
                    
                    o_new_vw = e_cloned["views"][s_new_wb_id][s_new_id]
                    print(f"Map old view '{o_vw.name}' to new view '{o_new_vw.name}")
    
    for s_proj_id, e_proj in e_source["child_projects"].items():
        if s_proj_id in e_old_new_maps:
            find_old_new_maps(
                e_proj,
                e_cloned["child_projects"][e_old_new_maps[s_proj_id]],
                e_old_new_maps,
                o_replacer,
                e_cloned_object_by_type=e_cloned_object_by_type,
                e_maps_by_type=e_maps_by_type,
                b_flows=b_flows,
                b_datasources=b_datasources,
                b_views=b_views,
                b_workbooks=b_workbooks,
                b_connections=b_connections,
            )


def delete_objects(o_server, e_objects_by_type, b_projects=False, b_flows=False, b_datasources=False, b_workbooks=False):
    
    if b_workbooks:
        for s_id in e_objects_by_type["workbooks"]:
            print(f"Deleting workbook {s_id}")
            o_server.workbooks.delete(s_id)
        
    if b_datasources:
        for s_id in e_objects_by_type["datasources"]:
            print(f"Deleting datasource {s_id}")
            o_server.datasources.delete(s_id)
        
    if b_flows:
        for s_id in e_objects_by_type["flows"]:
            print(f"Deleting flow {s_id}")
            o_server.flows.delete(s_id)
    
    if b_projects:
        for s_id in e_objects_by_type["projects"]:
            print(f"Deleting project {s_id}")
            o_server.projects.delete(s_id)


